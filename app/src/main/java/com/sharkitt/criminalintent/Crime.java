package com.sharkitt.criminalintent;

import java.util.UUID;

/**
 * Created by Men on 10.03.2018.
 */

public class Crime {

    private UUID mId;
    private String mTitle;
    public Crime() {
// Генерирование уникального идентификатора
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }
}
